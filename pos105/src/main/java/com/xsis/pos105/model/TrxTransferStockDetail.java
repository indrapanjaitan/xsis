package com.xsis.pos105.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name="pos_trx_transfer_stock_detail")
public class TrxTransferStockDetail {
	
	private long ID;
	private long transferId;
	private int instock;
	private long variantId;
	private int transferQty;
	private long createdBy;
	private Date createdOn;
	private long modifiedBy;
	private Date modifiedOn;

	public TrxTransferStockDetail() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	/**
	 * @return the iD
	 */
	@Id
	@Column(name = "id", nullable=false)
	public long getID() {
		return ID;
	}



	/**
	 * @param iD the iD to set
	 */
	public void setID(long iD) {
		ID = iD;
	}



	/**
	 * @return the transferId
	 */
	@Column(name="transfer_id", nullable=false)
	public long getTransferId() {
		return transferId;
	}



	/**
	 * @param transferId the transferId to set
	 */
	
	public void setTransferId(long transferId) {
		this.transferId = transferId;
	}



	/**
	 * @return the instock
	 */
	@Column(name="instock")
	public int getInstock() {
		return instock;
	}



	/**
	 * @param instock the instock to set
	 */
	public void setInstock(int instock) {
		this.instock = instock;
	}



	/**
	 * @return the variantId
	 */
	@Column(name="variant_id", nullable=false)
	public long getVariantId() {
		return variantId;
	}



	/**
	 * @param variantId the variantId to set
	 */
	public void setVariantId(long variantId) {
		this.variantId = variantId;
	}



	/**
	 * @return the transferQty
	 */
	@Column(name="transfer_qty", nullable=false)
	public int getTransferQty() {
		return transferQty;
	}



	/**
	 * @param transferQty the transferQty to set
	 */
	public void setTransferQty(int transferQty) {
		this.transferQty = transferQty;
	}



	/**
	 * @return the createdBy
	 */
	@Column(name = "created_by")
	public long getCreatedBy() {
		return createdBy;
	}



	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}



	/**
	 * @return the createdOn
	 */
	@Column(name = "created_on")
	public Date getCreatedOn() {
		return createdOn;
	}



	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}



	/**
	 * @return the modifiedBy
	 */
	@Column(name = "modified_by")
	public long getModifiedBy() {
		return modifiedBy;
	}



	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}



	/**
	 * @return the modifiedOn
	 */
	@Column(name = "modified_on")
	public Date getModifiedOn() {
		return modifiedOn;
	}



	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}



	@Override
	public String toString() {
		return "TransferStockDetail [ID=" + ID + ", transferId =" + transferId + ", instock =" + instock + ", variantId=" + variantId + ", transferQty=" + transferQty + ", createdOn=" + createdOn + ", createdBy=" + createdBy + ", modifiedBy= " + modifiedBy +", modifiedBy =" + modifiedBy + "]";
	}
	

}
