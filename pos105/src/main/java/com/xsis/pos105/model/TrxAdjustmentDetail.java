package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="POS_TRX_ADJUSTMENT_DETAIL")
public class TrxAdjustmentDetail {

	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue (strategy=GenerationType.TABLE, generator="AdjustmentDetail")
	@TableGenerator (name="AdjustmentDetail", table="T_SEQUENCE_105", pkColumnName="SEQ_NAME", pkColumnValue="AdjustmentDetail", valueColumnName="SEQ_VAL", allocationSize=1, initialValue=1)
	private long id;
	
	@Column(name="ADJUSTMENT_ID", nullable=false)
	private long adjustmentId;
	
	@Column(name="VARIANT_ID", nullable=false)
	private long variantId;
	
	@Column(name="IN_STOCK", nullable=false)
	private int inStock;
	
	@Column(name="ACTUAL_STOCK", nullable=false)
	private int actualStock;
	
	@Column(name="CREATED_BY", nullable=true)
	private long createdBy;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_ON", nullable=true)
	private Date createdOn;
	
	@Column(name="MODIFIED_BY", nullable=true)
	private long modifiedBy;
	
	@Temporal(TemporalType.DATE)
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifiedOn;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAdjustmentId() {
		return adjustmentId;
	}

	public void setAdjustmentId(long adjustmentId) {
		this.adjustmentId = adjustmentId;
	}

	public long getVariantId() {
		return variantId;
	}

	public void setVariantId(long variantId) {
		this.variantId = variantId;
	}

	public int getInStock() {
		return inStock;
	}

	public void setInStock(int inStock) {
		this.inStock = inStock;
	}

	public int getActualStock() {
		return actualStock;
	}

	public void setActualStock(int actualStock) {
		this.actualStock = actualStock;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Override
	public String toString() {
		return "AdjustmentDetail [id=" + id + ", adjustmentId=" + adjustmentId + ", variantId=" + variantId
				+ ", inStock=" + inStock + ", actualStock=" + actualStock + ", createdBy=" + createdBy + ", createdOn="
				+ createdOn + ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn + "]";
	}
	
	
	
}
