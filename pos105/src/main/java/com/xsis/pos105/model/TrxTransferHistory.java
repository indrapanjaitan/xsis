package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table (name="POS_TRX_TRANSFER_HISTORY")
public class TrxTransferHistory {
	
	private long id;
	private long transferId;
	private String status;
	private long createdBy;
	private Date createdOn;
	/**
	 * @return the id
	 */
	@Id
	@Column (name="ID", nullable=false)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "TransferHistory")
	@TableGenerator(name = "TransferHistory", table = "T_SEQUENCE_105", pkColumnName = "SEQ_NAME", pkColumnValue = "TransferHistory", valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the transferId
	 */
	@Column (name="TRANSFER_ID", nullable=false)
	public long getTransferId() {
		return transferId;
	}
	/**
	 * @param transferId the transferId to set
	 */
	public void setTransferId(long transferId) {
		this.transferId = transferId;
	}
	/**
	 * @return the status
	 */
	@Column (name="STATUS", nullable=false)
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the createdBy
	 */
	@Column (name="CREATED_BY")
	public long getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column (name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Override
	public String toString() {
		return "TransferHistory [id=" + id + ", transferId=" + transferId + ", status=" + status + ", createdBy="
				+ createdBy + ", createdOn=" + createdOn + "]";
	}
	

}
