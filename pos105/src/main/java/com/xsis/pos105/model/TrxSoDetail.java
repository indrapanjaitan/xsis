package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="POS_TRX_SO_DETAIL")
public class TrxSoDetail {

	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue (strategy=GenerationType.TABLE, generator="SoDetail")
	@TableGenerator (name="SoDetail", table="T_SEQUENCE_105", pkColumnName="SEQ_NAME", pkColumnValue="SoDetail", valueColumnName="SEQ_VAL", allocationSize=1, initialValue=1)
	private long id;
	
	@Column(name="SO_ID", nullable=false)
	private long soId;
	
	@Column(name="VARIANT_ID", nullable=false)
	private long variantId;
	
	@Column(name="QTY", nullable=false)
	private int qty;
	
	@Column(name="UNIT_PRICE", nullable=false)
	private double unitPrice;
	
	@Column(name="SUB_TOTAL", nullable=false)
	private double subTotal;
	
	@Column(name="CREATED_BY", nullable=true)
	private long createdBy;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_ON", nullable=true)
	private Date createdOn;
	
	@Column(name="MODIFIED_BY", nullable=true)
	private long modifiedBy;
	
	@Temporal(TemporalType.DATE)
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifiedOn;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getSoId() {
		return soId;
	}

	public void setSoId(long soId) {
		this.soId = soId;
	}

	public long getVariantId() {
		return variantId;
	}

	public void setVariantId(long variantId) {
		this.variantId = variantId;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Override
	public String toString() {
		return "SoDetail [id=" + id + ", soId=" + soId + ", variantId=" + variantId + ", qty=" + qty + ", unitPrice="
				+ unitPrice + ", subTotal=" + subTotal + ", createdBy=" + createdBy + ", createdOn=" + createdOn
				+ ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn + "]";
	}
	
	
}
