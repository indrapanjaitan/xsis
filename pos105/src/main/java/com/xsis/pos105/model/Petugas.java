package com.xsis.pos105.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_MST97_PETUGAS")
public class Petugas {
	
	private int id;
	private String kode;
	private String nama;
	private String alamat;
	private String email;
	private String password;
	private String noTelepon;
	
	@Id
	@Column(name="id", length=12)
	@GeneratedValue (strategy = GenerationType.TABLE, generator = "Petugas")
	@TableGenerator (name = "Petugas", table = "T_SEQUENCE_97", pkColumnName = "SEQ_NAME", pkColumnValue = "Petugas", valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="kode", length=10)
	public String getKode() {
		return kode;
	}
	public void setKode(String kode) {
		this.kode = kode;
	}
	
	@Column(name="nama", length=50)
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	@Column(name="alamat", length=50)
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	
	@Column(name="email", length=20)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Column(name="password", length=255)
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name="NoTelepon", length=20)
	public String getNoTelepon() {
		return noTelepon;
	}
	public void setNoTelepon(String noTelepon) {
		this.noTelepon = noTelepon;
	}
	
	


}
