package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "POS_TRX_PR_DETAIL")
public class TrxPrDetail {
	private long id;
	private long prId;
	private long variantId;
	private int requestQty;
	private long createdBy;
	private Date createdOn;
	private long modifiedBy;
	private Date modifiedOn;

	@Id
	@Column(name = "ID", nullable=false)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "PrDetail")
	@TableGenerator(name = "PrDetail", table = "T_SEQUENCE_105", pkColumnName = "SEQ_NAME", pkColumnValue = "PrDetail", valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "PR_ID", nullable=false)
	public long getPrId() {
		return prId;
	}

	public void setPrId(long prId) {
		this.prId = prId;
	}

	@Column(name = "VARIANT_ID", nullable=false)
	public long getVariantId() {
		return variantId;
	}

	public void setVariantId(long variantId) {
		this.variantId = variantId;
	}

	@Column(name = "REQUEST_QTY", nullable=false)
	public int getRequestQty() {
		return requestQty;
	}

	public void setRequestQty(int requestQty) {
		this.requestQty = requestQty;
	}

	@Column(name = "CREATED_BY")
	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "MODIFIED_BY")
	public long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Override
	public String toString() {
		return "PrDetail [id=" + id + ", prId=" + prId + ", variantId=" + variantId + ", requestQty=" + requestQty
				+ ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", modifiedBy=" + modifiedBy
				+ ", modifiedOn=" + modifiedOn + "]";
	}
}
