package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "POS_TRX_Adjustment")
public class TrxAdjustment {

	private long id;
	private long outletId;
	private String notes;
	private String status;
	private long createdBy;
	private Date createdOn;
	private long modifiedBy;
	private Date modifiedOn;
	private boolean active;
	

	/**
	 * @return the id
	 */
	@Id
	@Column(name = "id", length = 12, nullable = false)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "Adjustment")
	@TableGenerator(name = "Adjustment", table = "T_SEQUENCE_105", pkColumnName = "SEQ_NAME", pkColumnValue = "Adjustment", valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	
	/**
	 * @return the outletId
	 */
	@Column(name = "outletId", length = 12)
	public long getOutletId() {
		return outletId;
	}

	/**
	 * @param outletId the outletId to set
	 */
	public void setOutletId(long outletId) {
		this.outletId = outletId;
	}

	/**
	 * @return the notes
	 */
	@Column(name = "notes", length = 255, nullable = false)
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the status
	 */
	@Column(name = "status", length = 20)
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}


	/**
	 * @return the createdBy
	 */
	@Column(name = "createdBy", length = 12, nullable = false)
	public long getCreated_by() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreated_by(long createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "createdOn", nullable = false)
	public Date getCreated_on() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreated_on(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	@Column(name = "modifiedBy", length = 12, nullable = false)
	public long getModified_by() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModified_by(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "modifiedOn", nullable = false)
	public Date getModified_on() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModified_on(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * @return the active
	 */
	@Column(name = "is_active")
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

}
