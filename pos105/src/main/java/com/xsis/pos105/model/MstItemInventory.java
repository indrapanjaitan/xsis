package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="POS_MST_ITEM_INVENTORY")
public class MstItemInventory {
	private long id;
	private long variantId;
	private long outletId;
	private int begining;
	private int purchaseQty;
	private int salesOrderQty;
	private int transferStockQty;
	private int adjustmentQty;
	private int endingQty;
	private int alertAtQty;
	private long createdBy;
	private Date createdOn;
	private long modifiedBy;
	private Date modifiedOn;
	/**
	 * @return the id
	 */
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "ItemInventory")
	@TableGenerator(name = "ItemInventory", table = "T_SEQUENCE_105", pkColumnName = "SEQ_NAME", pkColumnValue = "ItemInventory", valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the variantId
	 */
	@Column(name="VARIANT_ID")
	public long getVariantId() {
		return variantId;
	}
	/**
	 * @param variantId the variantId to set
	 */
	public void setVariantId(long variantId) {
		this.variantId = variantId;
	}
	/**
	 * @return the outletId
	 */
	@Column(name="OUTLET_ID", nullable=false)
	public long getOutletId() {
		return outletId;
	}
	/**
	 * @param outletId the outletId to set
	 */
	public void setOutletId(long outletId) {
		this.outletId = outletId;
	}
	/**
	 * @return the begining
	 */
	@Column(name="BEGINING", nullable=false)
	public int getBegining() {
		return begining;
	}
	/**
	 * @param begining the begining to set
	 */
	public void setBegining(int begining) {
		this.begining = begining;
	}
	/**
	 * @return the purchaseQty
	 */
	@Column(name="PURCHASE_QTY")
	public int getPurchaseQty() {
		return purchaseQty;
	}
	/**
	 * @param purchaseQty the purchaseQty to set
	 */
	public void setPurchaseQty(int purchaseQty) {
		this.purchaseQty = purchaseQty;
	}
	/**
	 * @return the salesOrderQty
	 */
	@Column(name="SALES_ORDER_QTY")
	public int getSalesOrderQty() {
		return salesOrderQty;
	}
	/**
	 * @param salesOrderQty the salesOrderQty to set
	 */
	public void setSalesOrderQty(int salesOrderQty) {
		this.salesOrderQty = salesOrderQty;
	}
	/**
	 * @return the transferStockQty
	 */
	@Column(name="TRANSFER_STOCK_QTY")
	public int getTransferStockQty() {
		return transferStockQty;
	}
	/**
	 * @param transferStockQty the transferStockQty to set
	 */
	public void setTransferStockQty(int transferStockQty) {
		this.transferStockQty = transferStockQty;
	}
	/**
	 * @return the adjustmentQty
	 */
	@Column(name="ADJUSTMENT_QTY")
	public int getAdjustmentQty() {
		return adjustmentQty;
	}
	/**
	 * @param adjustmentQty the adjustmentQty to set
	 */
	public void setAdjustmentQty(int adjustmentQty) {
		this.adjustmentQty = adjustmentQty;
	}
	/**
	 * @return the endingQty
	 */
	@Column(name="ENDING_QTY", nullable=false)
	public int getEndingQty() {
		return endingQty;
	}
	/**
	 * @param endingQty the endingQty to set
	 */
	public void setEndingQty(int endingQty) {
		this.endingQty = endingQty;
	}
	/**
	 * @return the alertAtQty
	 */
	@Column(name="ALERT_AT_QTY", nullable=false)
	public int getAlertAtQty() {
		return alertAtQty;
	}
	/**
	 * @param alertAtQty the alertAtQty to set
	 */
	public void setAlertAtQty(int alertAtQty) {
		this.alertAtQty = alertAtQty;
	}
	/**
	 * @return the createdBy
	 */
	@Column(name="CREATED_BY")
	public long getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the modifiedBy
	 */
	@Column(name="MODIFIED_BY")
	public long getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the modifiedOn
	 */
	@Column(name="MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}
	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ItemInventory [id=" + id + ", variantId=" + variantId + ", outletId=" + outletId + ", begining="
				+ begining + ", purchaseQty=" + purchaseQty + ", salesOrderQty=" + salesOrderQty + ", transferStockQty="
				+ transferStockQty + ", adjustmentQty=" + adjustmentQty + ", endingQty=" + endingQty + ", alertAtQty="
				+ alertAtQty + ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", modifiedBy=" + modifiedBy
				+ ", modifiedOn=" + modifiedOn + "]";
	}
	
	

}
