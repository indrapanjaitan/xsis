package com.xsis.pos105.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


//import com.xsis.pos105.viewmodel.MstEmployeeViewModel;
import com.xsis.pos105.dao.EmployeeDao;
import com.xsis.pos105.model.MstEmployee;
import com.xsis.pos105.service.EmployeeService;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService{
	
	@Autowired
	private EmployeeDao employeeDao;
	
//	@Autowired
//	private EmployeeDao dao;
//	@Override
//	public Petugas getByUserName(String email) throws Exception {
//		// TODO Auto-generated method stub
//		return employeeDao.getByUserName(email);
//	}

	@Override
	public List<MstEmployee> get() throws Exception {
		// TODO Auto-generated method stub
		return this.employeeDao.get();
	}

	@Override
	public List<MstEmployee> search(String keySearch) throws Exception {
		// TODO Auto-generated method stub
		return this.employeeDao.search(keySearch);
	}

	@Override
	public MstEmployee getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.employeeDao.getById(id);
	}

	@Override
	public MstEmployee getByEmail(String email) throws Exception {
		// TODO Auto-generated method stub
		return this.employeeDao.getByEmail(email);
	}

//	@Override
//	public void insert(MstEmployeeViewModel model) throws Exception {
//		this.dao.insert(model);
//	}
	
	@Override
	public void update(MstEmployee model) throws Exception {
		// TODO Auto-generated method stub
		this.employeeDao.update(model);
	}

	@Override
	public void delete(MstEmployee model) throws Exception {
		// TODO Auto-generated method stub
		this.employeeDao.delete(model);
	}
}
