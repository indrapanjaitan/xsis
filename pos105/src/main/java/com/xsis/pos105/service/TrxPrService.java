package com.xsis.pos105.service;

import java.util.Collection;

import com.xsis.pos105.model.TrxPr;


public interface TrxPrService {
	public Collection<TrxPr> getAll() throws Exception;
	public TrxPr getById(int id) throws Exception;
	public void save(TrxPr pr) throws Exception;
	public void update(TrxPr pr) throws Exception;
	public void delete(TrxPr pr) throws Exception;

}
