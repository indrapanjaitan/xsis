package com.xsis.pos105.service;

import java.util.Collection;

import com.xsis.pos105.model.TrxPrHistory;

public interface TrxPrHistoryService {
	public Collection<TrxPrHistory> getAll() throws Exception;
	public TrxPrHistory getById(int id) throws Exception;
	public void save(TrxPrHistory prhistory) throws Exception;
	public void update(TrxPrHistory prhistory) throws Exception;
	public void delete(TrxPrHistory prhistory) throws Exception;

}
