package com.xsis.pos105.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.pos105.dao.MstSupplierDao;
import com.xsis.pos105.model.MstSupplier;
import com.xsis.pos105.service.MstSupplierService;

@Service
@Transactional
public class MstSupplierServiceImpl implements MstSupplierService {

	@Autowired
	private MstSupplierDao supplierDao;
	
	@Override
	public Collection<MstSupplier> getAll() throws Exception {
		return supplierDao.getAll();
	}

	@Override
	public MstSupplier getById(int id) throws Exception {
		return supplierDao.getById(id);
	}

	@Override
	public void save(MstSupplier mstSupplier) throws Exception {
		supplierDao.save(mstSupplier);
	}

	@Override
	public void update(MstSupplier mstSupplier) throws Exception {
		supplierDao.update(mstSupplier);
	}

	@Override
	public void delete(MstSupplier mstSupplier) throws Exception {
		supplierDao.delete(mstSupplier);
	}

}
