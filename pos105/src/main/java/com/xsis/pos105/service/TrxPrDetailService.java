package com.xsis.pos105.service;

import java.util.Collection;

import com.xsis.pos105.model.TrxPrDetail;

public interface TrxPrDetailService {
	public Collection<TrxPrDetail> getAll() throws Exception;
	public TrxPrDetail getById(int id) throws Exception;
	public void save(TrxPrDetail prdetail) throws Exception;
	public void update(TrxPrDetail prdetail) throws Exception;
	public void delete(TrxPrDetail prdetail) throws Exception;

}
