package com.xsis.pos105.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.pos105.dao.TrxPoDetailDao;
import com.xsis.pos105.model.TrxPoDetail;
import com.xsis.pos105.service.TrxPoDetailService;

@Service
@Transactional
public class TrxPoDetailServiceImpl implements TrxPoDetailService{
	@Autowired
	private TrxPoDetailDao trxPoDetailDao;
	
	@Override
	public Collection<TrxPoDetail> getAll() throws Exception {
		// TODO Auto-generated method stub
		return trxPoDetailDao.getAll();
	}

	@Override
	public TrxPoDetail getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return trxPoDetailDao.getById(id);
	}

	@Override
	public void save(TrxPoDetail trxPoDetail) throws Exception {
		// TODO Auto-generated method stub
		trxPoDetailDao.save(trxPoDetail);
	}

	@Override
	public void update(TrxPoDetail trxPoDetail) throws Exception {
		// TODO Auto-generated method stub
		trxPoDetailDao.update(trxPoDetail);
	}

	@Override
	public void delete(TrxPoDetail trxPoDetail) throws Exception {
		// TODO Auto-generated method stub
		trxPoDetailDao.delete(trxPoDetail);
	}

}
