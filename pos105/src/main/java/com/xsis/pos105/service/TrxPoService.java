package com.xsis.pos105.service;

import java.util.Collection;

import com.xsis.pos105.model.TrxPo;

public interface TrxPoService {
	public Collection<TrxPo> getAll() throws Exception;
	public TrxPo getById(int id) throws Exception;
	public void save(TrxPo trxPo) throws Exception;
	public void update(TrxPo trxPo) throws Exception;
	public void delete(TrxPo trxPo) throws Exception;
	
}
