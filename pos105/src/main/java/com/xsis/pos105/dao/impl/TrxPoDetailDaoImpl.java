package com.xsis.pos105.dao.impl;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.pos105.dao.TrxPoDetailDao;
import com.xsis.pos105.model.TrxPoDetail;

@Repository
public class TrxPoDetailDaoImpl implements TrxPoDetailDao{
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<TrxPoDetail> getAll() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from TrxPoDetail";
		Query query = session.createQuery(hql);
		return query.list();
	}

	@Override
	public TrxPoDetail getById(int id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from TrxPoDetail where id=:id";
		Query query = session.createQuery(hql);
		return (TrxPoDetail) query.list().get(0);
	}

	@Override
	public void save(TrxPoDetail trxPoDetail) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.save(trxPoDetail);
	}

	@Override
	public void update(TrxPoDetail trxPoDetail) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.update(trxPoDetail);
	}

	@Override
	public void delete(TrxPoDetail trxPoDetail) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.delete(trxPoDetail);
	}

}
