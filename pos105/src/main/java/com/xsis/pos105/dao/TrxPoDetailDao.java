package com.xsis.pos105.dao;

import java.util.Collection;

import com.xsis.pos105.model.TrxPoDetail;

public interface TrxPoDetailDao {
	public Collection<TrxPoDetail> getAll() throws Exception;
	public TrxPoDetail getById(int id) throws Exception;
	public void save(TrxPoDetail trxPoDetail) throws Exception;
	public void update(TrxPoDetail trxPoDetail) throws Exception;
	public void delete(TrxPoDetail trxPoDetail) throws Exception;
}
