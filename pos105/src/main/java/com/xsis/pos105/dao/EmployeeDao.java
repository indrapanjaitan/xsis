package com.xsis.pos105.dao;

import java.util.List;

//import com.xsis.pos105.viewmodel.MstEmployeeViewModel;
import com.xsis.pos105.model.MstEmployee;

public interface EmployeeDao {
	
	
	public List<MstEmployee> get() throws Exception;
	public List<MstEmployee> search(String keySearch) throws Exception;
	public MstEmployee getById(int id) throws Exception;
	public MstEmployee getByEmail(String email) throws Exception;
	//public void insert(MstEmployeeViewModel model) throws Exception;
	public void update(MstEmployee model) throws Exception;
	public void delete(MstEmployee model) throws Exception;
}
