package com.xsis.pos105.dao.impl;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xsis.pos105.dao.TrxTransferStockDao;
import com.xsis.pos105.model.TrxTransferStock;

public class TrxTransferStockDaoImpl implements TrxTransferStockDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Collection<TrxTransferStock> getAll() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from TransferStock";
		Query query = session.createQuery(hql);
		return query.list();
	}

	@Override
	public TrxTransferStock getById(int id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from TransferStock where id=:id";
		Query query = session.createQuery(hql);
		return (TrxTransferStock) query.list().get(0);
	}

	@Override
	public void save(TrxTransferStock transferStock) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.save(transferStock);
	}

	@Override
	public void update(TrxTransferStock transferStock) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.update(transferStock);
	}

	@Override
	public void delete(TrxTransferStock transferStock) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.delete(transferStock);
	}

}
