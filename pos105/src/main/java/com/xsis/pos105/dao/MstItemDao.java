package com.xsis.pos105.dao;

import java.util.Collection;

import com.xsis.pos105.model.MstItem;

public interface MstItemDao {

	public Collection<MstItem> getAll() throws Exception;
	public MstItem getById(int id) throws Exception;
	public void save(MstItem mstItem) throws Exception;
	public void update(MstItem mstItem) throws Exception;
	public void delete(MstItem mstItem) throws Exception;
}
