package com.xsis.pos105.dao;

import java.util.Collection;

import com.xsis.pos105.model.TrxTransferStock;

public interface TrxTransferStockDao {

	public Collection<TrxTransferStock> getAll() throws Exception;
	public TrxTransferStock getById(int id) throws Exception;
	public void save(TrxTransferStock transferStock) throws Exception;
	public void update(TrxTransferStock transferStock) throws Exception;
	public void delete(TrxTransferStock transferStock) throws Exception;
}
