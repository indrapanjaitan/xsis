package com.xsis.pos105.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;

import com.xsis.pos105.model.MstEmployee;
import com.xsis.pos105.service.EmployeeService;

@Controller
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value="/master/employee")
	public ModelAndView index(){
		return new ModelAndView("/master/employee");
	}
	
//	@RequestMapping(value="/master/employee/list")
//	public ModelAndView list(Model model){
//		//List<MstEmployee> result= new ArrayList<MstEmployee>();
//		List<MstEmployee> result = new ArrayList();
//		try{
//			result=this.employeeService.get();
//		}catch (Exception e){
//			
//		}
//		model.addAttribute("list",result);
//		return new ModelAndView("/master/employee/list");
////		return new ModelAndView("/master/list");
//	}
}
