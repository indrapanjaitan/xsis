/**
 * 
 */
package com.xsis.pos105.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;

import com.xsis.pos105.model.Petugas;
import com.xsis.pos105.service.EmployeeService;

/**
 * @author Awiyanto Ajisasongko
 *
 */
@Controller
public class BaseController {

	@Autowired
	EmployeeService employeeService;

	public String getUserName() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			for (GrantedAuthority a : auth.getAuthorities()) {
				System.out.println(a);
			}

			return userDetail.getUsername();
		}
		return null;
	}

	public int getUserId() {
		String userName = getUserName();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth!=null) {
			User user = (User) auth.getPrincipal();
			return 0;
		}
		return 0;
	}
	public int getOutletId(HttpSession session){
		int outletId = 0;
		if(!session.getAttribute("outletId").toString().equals("")){
			outletId = Integer.parseInt(session.getAttribute("outletId").toString());
		}
		return outletId;
	}
}
